1. NOTE: There is an existing folder 000-TESTING where all files should be storeed.
2. Option 1 - Local VS Code - Clone this repo - https://gitlab.com/sierra-cedar-public/flexops-training-lab.git locally
3. Option 2 - Use Gitlab Web IDE
4. Create a new branch from "main" with following naming conventions:  add-s3-bucket-your-initials # change your-initials to your own
5. Add a file with the same naming convention as the branch with .tf extension (i.e. ) 000-TESTING/add-s3-bucket-your-initials.tf
6. Make sure all files have replaced the literal "your-initials" and "your-name" with your actual initials/name.
```
resource "aws_s3_bucket" "your-initials-bucket" {
  bucket_prefix = "your-initials-bucket"

  tags = {
    Name        = "your-name"
    Environment = "your-name"
  }
}
```
4. Change all references of your-initials and your-name to yours within code block
5. Stage files using VSCode or Git commands
6. Commit files
7. Optional for VSCode: Push branch to Remote
8. Create an MR in Gitlab and merge into "main"
