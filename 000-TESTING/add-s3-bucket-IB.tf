resource "aws_s3_bucket" "IB-bucket" {
  bucket_prefix = "iv-bucket"

  tags = {
    Name        = "IvoBilikha"
    Environment = "IvoBilikha"
  }
}
