resource "aws_s3_bucket" "mk2-bucket-s3" {
  bucket_prefix = "mk2-bucket"

  tags = {
    Name        = "murthyk2"
    Environment = "murthyk2"
  }
}
