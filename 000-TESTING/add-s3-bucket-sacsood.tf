resource "aws_s3_bucket" "sacsood-bucket" {
  bucket_prefix = "sacsood-bucket"

  tags = {
    Name        = "Sachin Sood"
    Environment = "Sachin Sood"
  }
}
