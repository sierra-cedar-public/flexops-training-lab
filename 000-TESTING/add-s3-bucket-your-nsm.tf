resource "aws_s3_bucket" "your-initials-bucket" {
  bucket_prefix = "nsm-bucket"

  tags = {
    Name        = "Navjot Mann"
    Environment = "Navjot Mann"
  }
}
