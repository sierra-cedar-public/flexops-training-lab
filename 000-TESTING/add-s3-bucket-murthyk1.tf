resource "aws_s3_bucket" "murthy-s3" {
  bucket_prefix = "murthy"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}
