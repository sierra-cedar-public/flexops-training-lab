resource "aws_s3_bucket" "laxman-s3" {
  bucket_prefix = "laxman"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}