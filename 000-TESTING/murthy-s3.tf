resource "aws_s3_bucket" "mk1-bucket-s3" {
  bucket_prefix = "mk1-bucket"

  tags = {
    Name        = "murthyk1"
    Environment = "murthyk1"
  }
}

